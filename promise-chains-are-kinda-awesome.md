---
title: Promise Chains are Kinda Awesome
published: false
description: Some say that promises shouldn't be used when async/await is available, but that's not always true. Here's why "promise chains" are actually amazing.
tags: Promise, async, Functional, JavaScript
---

Oh you came here for the promises? Yeah we'll get to that in a second, but first let me introduce you to a buddy of mine called Trace

```js
const trace = tag => x =>
  console.log(tag, x) || x;
```

We met at this [@drBoolean](https://mostly-adequate.gitbooks.io/mostly-adequate-guide/ch05.html#debugging) jam a few years back and sorta hit it off. I realised we have a lot in common: we both have a strong sense of identity, but are not afraid to effect a little change on the side when called for. Kid makes a mean curry too.

See, thing about Trace is, he doesn't mind where you put him, he's happy just to do his own thing. Kind of goes with the flow, promise!

```js
['a', 'b', 'c']
  .map(trace('what do we have here...'))

// what do we have here ... a
// what do we have here ... b
// what do we have here ... c

fetch(`/users`)
  .then(handleAsJson)
  .then(trace('all users: '))

// all users: [{ id: 1, isAdmin: false }, { id: 2, isAdmin: true }]

const fancyAdd = y => compose(
  trace('after fancy'),
  x => x + y,
  trace('before fancy'),
  x => x + 1
)
```

```js
const filter = f => xs => Array.isArray(xs) ? xs.filter(f) : xs;
const isAdmin = ({isAdmin = false}) => isAdmin;
const fetchUserAvatar = ({id}) => fetch(`/user-media/avatars/${id}.svg`)

fetch('/users')
  .then(handleAsJson)
  .then(filter(isAdmin))
  .then(fetchUserAvatar) // [Promise<user>]
  .then(Promise.all)     // Promise<[user]>
```

- passing well-named callbacks to then first-class leads to code that reads like prose
- variables can be preserved using closures
- lambdas are not always necessary
- async/await is certainly helpful even in functional flows, especially when the number of closures gets out of hand, or to quickly return a promise.
  - but perhaps that is a smell of a larger architectural problem.
- importantly, imperative async/await blocks cannot be composed.
